#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""speeddating.py: Processes all speeddating choices and sends an SMS informing each participant of their matches."""

import csv
import sys

from participant import Participant
from smsgateway import SmsgatewayDevice, SmsgatewayAccount
from speeddate import SpeedDate

__author__ = "Maarten Desnouck"
__license__ = "GPL"
__version__ = "1.0.2"
__maintainer__ = "Maarten Desnouck"
__email__ = "maarten.desnouck@gmail.com"
__status__ = "Development"
__date__ = "18 Jan, 2016"

# constanten definiëren
MAX_MATCHES = 5
USERNAME = ""
PASSWORD = ""
DEVICEID = ""
addPhonenumbers = False

# Deelnemers inlezen
participants = []
with open('deelnemers.csv', 'rb') as deelnemers:
    reader = csv.reader(deelnemers)
    deDeelnemers = list(reader)

for deelnemer in deDeelnemers:
    # Voornaam,Achternaam,Geslacht,Interesse,Kring,Jaar,E-mailadres,+TelNr,id
    participants.append(Participant(int(deelnemer[8]), deelnemer[
                        0], deelnemer[1], deelnemer[7]))

# VTKSpeedDate aanmaken
afsluiter = " Met vriendelijke groeten van het Speeddating-team."
VTKSpeedDate = SpeedDate(afsluiter, participants)

# Keuzes inlezen
with open('keuzes.csv', 'rb') as keuzes:
    reader = csv.reader(keuzes)
    deKeuzes = list(reader)

for keuze in deKeuzes:
    keuzeLijst = []
    for i in range(1, MAX_MATCHES + 1):
        keuzeLijst.append(keuze[i])
    VTKSpeedDate.choose(keuze[0], keuzeLijst)
print("Alle keuzes zijn correct ingeladen.\n")

# Opvragen welke smsen we moeten versturen
messages = VTKSpeedDate.getMessages(addPhonenumbers)

# SMSgateway.me account en gsm toestel aanmaken
account = SmsgatewayAccount(USERNAME, PASSWORD)
GSM = SmsgatewayDevice(account, DEVICEID)

# Vragen of we de smsen mogen versturen (je kan dus nog checken of ze er
# goed uitzien)
send = str(raw_input('\nSend all created SMS messages? (Y|N) '))

# Smsen al dan niet verzenden
if send.lower() == 'y':
    for message in messages:
        GSM.send(message[0].decode('utf-8'), message[1].decode('utf-8'))

    print("\nThe SMS messages in buffer have been sent to SMSgateway.me. Make sure the app on your phone is doing the rest.")
else:
    print("\nThe SMS messages in buffer have been CANCELLED!")
