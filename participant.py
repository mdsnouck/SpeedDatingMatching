#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Deelnemer van een speeddate
class Participant():
    id = ""
    first_name = ""
    last_name = ""
    phoneNumber = ""

    # Constructor
    def __init__(self, id, first_name, last_name, phoneNumber):
        self.id = id
        self.first_name = first_name
        self.last_name = last_name
        self.phoneNumber = phoneNumber

    # Geeft de volle naam terug; is mooier in de oproepende code
    def fullName(self):
        return self.first_name+" "+self.last_name
